FROM php:7.2.1-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

MAINTAINER wheelet1228@gmail.com

RUN docker-php-ext-install pdo pdo_mysql mysqli bcmath

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip && \
    apt-get install -y libzip-dev

# PHP Extension: Zip
RUN pecl install zip
RUN echo "extension=zip.so" >> /usr/local/lib/php.ini

# Set environmental variables
ENV COMPOSER_HOME /root/composer
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Display Composer version information
RUN composer --version

RUN a2enmod rewrite

RUN chown -R www-data:www-data /var/www
