<?php


namespace App;

use InvalidArgumentException;
use Strategy\StrategyInterface;

/**
 * Class AbstractBlock
 * @package App
 */
abstract class AbstractBlock
{
    /**
     * @var string
     */
    protected $className;
    protected $content;
    protected $title;

    /**
     * @var StrategyInterface
     */
    protected $strategy;

    /**
     * @var array
     */
    protected $fields = array();
    public static $treeComposite = array();
    public static $nameOfClass = array();

    /**
     * AbstractBlock constructor.
     * @param $className
     * @param string $title
     */
    public function __construct($className, $title = '')
    {
        $this->className = $className;
        $this->title = $title;
        self::$treeComposite[] = $this;
    }

    /**
     * @param $array
     * @param string $tab
     * @param string $result
     * @return string
     */
    public static function getTree($array, $tab = '', $result = "") {
        foreach ($array as $key => $var)
        {
            if ($var instanceof Image || $var instanceof Button){
                $result .= "{$tab}[$key] => <b>{$var->getNameOfClass()}</b><br>";
            }else{
                $result .= "{$tab}[$key] <i style='color:green;'>{$var->getNameOfClass()}</i><br>";
                $result .= self::getTree($var->getChildren(), $tab . str_repeat('&nbsp;', 4));
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getNameOfClass(): string
    {
        return static::class;
    }

    /**
     * @return mixed
     */
    abstract public function render():void;

    /**
     * @return mixed
     */
    abstract public function renderComposition();

    /**
     * @return mixed
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className)
    {
        $this->className = $className;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return strlen($this->content);
    }

    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return spl_object_hash($this);
    }

    /**
     * Template method addNewContent($block, $tagAfterBlock)
     *
     * @param $block
     * @param $tagAfterBlock
     */
    final public function addNewContent($block, $tagAfterBlock):void
    {
        if ($block == "" && $tagAfterBlock == "")
            throw new InvalidArgumentException('Arguments won`t be empty');

        if (is_object($block) && $block instanceof AbstractBlock){
            if(is_object($tagAfterBlock) && $tagAfterBlock instanceof AbstractBlock){
                $this->prefix($block->content);
                $this->postfix($tagAfterBlock->content);
            }else{
                $this->prefix($block->content);
                $this->postfix($tagAfterBlock);
            }
        }else {
            if(is_object($tagAfterBlock) && $tagAfterBlock instanceof AbstractBlock){
                $this->prefix($block);
                $this->postfix($tagAfterBlock->content);
            }else{
                $this->prefix($block);
                $this->postfix($tagAfterBlock);
            }
        }

    }

    /**
     * @param $str
     */
    protected function prefix($str): void
    {
        echo "$str{$this->renderComposition()}";
    }

    /**
     * @param $str
     */
    protected function postfix($str): void
    {
        echo $str;
    }

    /**
     * @param StrategyInterface $strategy
     * @return string
     */
    public function setStrategy(StrategyInterface $strategy): string
    {
        $this->strategy = $strategy;
        self::$nameOfClass[] = get_class($this->strategy);

        return $this->strategy->plugin($this);
    }

    /**
     * @return string
     */
    public function getNameStrategy(): string
    {
        return implode(",",self::$nameOfClass);
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->fields;
    }
}