<?php

namespace App;

/**
 * Class Button
 * @package App
 */
class Button extends AbstractBlock
{
    public function render(): void
    {
        $this->content = "<button class=\"{$this->getClassName()}\">{$this->getTitle()}</button>";

        echo $this->content;
    }

    /**
     * @return string
     */
    public function renderComposition(): string
    {
        $this->content = "<button class=\"{$this->getClassName()}\">{$this->getTitle()}</button>";

        return $this->content;
    }
}