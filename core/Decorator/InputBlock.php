<?php

namespace Decorator;

/**
 * Class InputBlock
 * @package Decorator
 */
class InputBlock implements DecoratorInterface
{
    /**
     * @param \App\AbstractBlock $block
     * @return string
     */
    public function changeBlock($block): string
    {
        return $block->getContent();
    }
}