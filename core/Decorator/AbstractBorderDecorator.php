<?php

namespace Decorator;

/**
 * Class AbstractBorderDecorator
 * @package Decorator
 */
abstract class AbstractBorderDecorator implements DecoratorInterface
{
    protected $width;
    protected $color;
    protected $blockDecorator;

    /**
     * AbstractBorderDecorator constructor.
     * @param DecoratorInterface $blockDecorator
     * @param $width
     * @param $color
     */
    public function __construct(DecoratorInterface $blockDecorator, $width, $color)
    {
        $this->blockDecorator = $blockDecorator;
        $this->width = $width;
        $this->color = $color;
    }
}