<?php

namespace Decorator;

/**
 * Class BlockComments
 * @package Decorator
 */
class BlockComments extends FormatBlock
{
    /**
     * @param \App\AbstractBlock $block
     * @return string
     */
    public function changeBlock($block): string
    {
        $content = parent::changeBlock($block);
        $blockBegin = '<!-- AbstractBlock BEGIN. Type: '.$block->getClassName().', ID: '.$block->getObjectId().', Length: '
            .$block->getLength().' -->';
        $blockEnd = '<!-- AbstractBlock END. Type: '.$block->getClassName().', ID: '.$block->getObjectId().' -->';

        return $blockBegin.$content.$blockEnd;
    }
}