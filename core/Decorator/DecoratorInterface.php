<?php

namespace Decorator;

use App\AbstractBlock;

/**
 * Interface DecoratorInterface
 * @package Decorator
 */
interface DecoratorInterface
{
    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function changeBlock(AbstractBlock $block): string;
}