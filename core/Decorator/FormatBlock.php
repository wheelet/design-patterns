<?php

namespace Decorator;

/**
 * Class FormatBlock
 * @package Decorator
 */
class FormatBlock implements DecoratorInterface
{
    /**
     * @var DecoratorInterface
     */
    protected $blockDecorator;

    /**
     * FormatBlock constructor.
     * @param DecoratorInterface $blockDecorator
     */
    public function __construct(DecoratorInterface $blockDecorator)
    {
        $this->blockDecorator = $blockDecorator;
    }

    /**
     * @param \App\AbstractBlock $block
     * @return string
     */
    public function changeBlock($block): string
    {
        return $this->blockDecorator->changeBlock($block);
    }
}
