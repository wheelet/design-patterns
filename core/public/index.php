<?php
require_once '../vendor/autoload.php';

use App\AbstractBlock;
use App\Button;
use App\Image;
use App\Text;
use Decorator\BlockBorder;
use Decorator\BlockComments;
use Decorator\InputBlock;

$content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor
e magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequ
at. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sin
t occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ';

$button= new Button('a', 'zsgsddfg');
$image = new Image('as', 'zsgsddfg');
$text = new Text('class1', $content);

$button->render();
$image->render();
$text->render();

$button->setTitle("Exit");
$image->setTitle("Picture");
$text->setTitle("Hello world!");

$button->render();
$image->render();
$text->render();


$naiveInput = new InputBlock;
$filterInput = new BlockBorder($naiveInput, 5, 'red');
$filteredInput = new BlockComments($filterInput);

echo $filteredInput->changeBlock($button);

$text1 = new Text('class1', 'PAY');
$text2 = new Text('class2', 'CORE');
$button1= new Button('azdv', 'zsgsddfg');

$text->add($button);

$text->add($button1);
$text2->add($image);
$text->add($text2);

$text1->add($button);

echo $text1->renderComposition();
echo $text->renderComposition();
echo $text1->renderComposition();

echo AbstractBlock::getTree(AbstractBlock::$treeComposite);

$text->renderPlaceholder(['App\Button', 'App\Text']);

$text->addNewContent("sf","asd");

echo $text->setStrategy(new \Strategy\StrategyOne());
echo $text1->setStrategy(new \Strategy\StrategyTwo(['App\Button', 'App\Text']));