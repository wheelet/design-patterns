<?php
namespace Strategy;

use App\AbstractBlock;

/**
 * Class StrategyOne
 * @package Strategy
 */
class StrategyOne implements StrategyInterface
{
    /**
     * @param AbstractBlock $block
     * @return string
     */
    final public function plugin(AbstractBlock $block): string
    {
        $prefix = $this->prefix($block);
        $postfix = $this->postfix($block);

        return $prefix.$postfix;
    }

    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function prefix(AbstractBlock $block): string
    {
        return "<!-- AbstractBlock BEGIN. Type: {$block->getClassName()}, ID: {$block->getObjectId()}, Length: 
            {$block->getLength()} --> {$block->renderComposition()}";
    }

    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function postfix(AbstractBlock $block): string
    {
        return "<!-- AbstractBlock END. Type: {$block->getClassName()}, ID: {$block->getObjectId()}, strategy: {$block->getNameStrategy()}-->";
    }
}